﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SearcFile
{
    [Serializable]
    public class SaveData
    {
        public string StartDirectory { get; set; }
        public string TemplateNameFile { get; set; }
        public string TextFile { get; set; }

        public SaveData () //консруктор без параметров
        {

        }

        public SaveData(string _startDirectory, string _templateNameFile, string _textFile)
        {
            StartDirectory = _startDirectory;
            TemplateNameFile = _templateNameFile;
            TextFile = _textFile;
        }
    }
}
