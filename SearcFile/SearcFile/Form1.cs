﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace SearcFile
{
    public partial class SearchForm : Form
    {
        public SearchForm()
        {
            InitializeComponent();
        }
        
        private void btnPath_Click(object sender, EventArgs e) //выбрать путь
        {
            if (fBrowserDialog_Path.ShowDialog() == DialogResult.OK)
            {
                txtBoxStartDirectory.Text = fBrowserDialog_Path.SelectedPath;
            }
        }
        DirectoryInfo dirTest = null;

        private void btnSearch_Click(object sender, EventArgs e) //поиск
        {
            //int nod = 0;
            treeViewSearch.Nodes.Clear();

            DirectoryInfo directoryInfo = new DirectoryInfo(txtBoxStartDirectory.Text);
            treeViewSearch.BeginUpdate();
            Task task = Task.Factory.StartNew(() => ProcessingFile(directoryInfo));
            task.Wait();
            Stopwatch sw = new Stopwatch();
            sw.Start();
            int nod = Search(directoryInfo, treeViewSearch.Nodes);
            sw.Stop();
            if (nod == 0)
            {
                MessageBox.Show("Объект не найден!!");
                treeViewSearch.Nodes.Clear();
            }

            txtBoxTime.Text = (sw.ElapsedMilliseconds / 100.0).ToString();
            treeViewSearch.EndUpdate();
        }
        int number = 0;
        private void ProcessingFile(DirectoryInfo directory)
        {
            try
            {

                foreach (FileInfo file in directory.GetFiles()) //список файлов в каталоге
                {
                    txtBoxProcessingFile.Text = file.Name;
                    number++;
                    txtBoxNumberFile.Text = number.ToString();
                }

                foreach (DirectoryInfo dir in directory.GetDirectories()) //поиск в подпапках
                {
                    ProcessingFile(dir);
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        private int Search(DirectoryInfo dir, TreeNodeCollection treeNodeCollection)
        {

            int count = 0;
            TreeNode curNode = treeNodeCollection.Add("Folder", dir.Name); //добавляем узел
            string readFile;
            try
            {
                foreach (DirectoryInfo subdir in dir.GetDirectories()) //поиск в подпапках
                {
                    Search(subdir, curNode.Nodes);
                }

                foreach (FileInfo file in dir.GetFiles()) //список файлов в каталоге
                {
                    if (file.Name.EndsWith(txtBoxTemplateNameFile.Text)) //если шаблон имени файла совпадает с файлом в папке
                    {
                        StreamReader streamReader = new StreamReader(file.FullName, System.Text.Encoding.Default);

                        while ((readFile = streamReader.ReadLine()) != null) //считываем построчно
                        {
                            if (readFile.IndexOf(txtBoxTextFile.Text) > 0) // проверяем, есть ли такое слово в файле
                            {
                                count++;
                                //Добавляем в treeView
                                if (dirTest != dir)
                                {
                                    curNode.Nodes.Add("File", file.Name);
                                    dirTest = dir;
                                }
                                else
                                {
                                    curNode.Nodes.Add("File", file.Name);
                                }
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка поиска файлов! " + ex.Message);
                return 0;
            }

            return count;
        }

        private void SearchForm_Load(object sender, EventArgs e)
        {
            // передаем в конструктор тип класса
            XmlSerializer xml = new XmlSerializer(typeof(SaveData));
            //Десериализация
            FileStream fs = new FileStream("save.xml", FileMode.OpenOrCreate);
            if (fs.Length!=0)
            {
                SaveData data = (SaveData)xml.Deserialize(fs);
                txtBoxStartDirectory.Text = data.StartDirectory;
                txtBoxTemplateNameFile.Text = data.TemplateNameFile;
                txtBoxTextFile.Text = data.TextFile;
            }            
            fs.Close();
        }

        private void SearchForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            //Объект для сериализации
            SaveData saveData = new SaveData(txtBoxStartDirectory.Text, txtBoxTemplateNameFile.Text, txtBoxTextFile.Text);

            XmlSerializer xml = new XmlSerializer(typeof(SaveData));

            //запись
            FileStream fs = new FileStream("save.xml", FileMode.Create);
            xml.Serialize(fs, saveData);
            fs.Close();

        }

        private void btnResume_Click(object sender, EventArgs e)
        {
            //backgroundWorkerSearch.RunWorkerAsync(); //запускаем поток
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
           // backgroundWorkerSearch.CancelAsync();
        }

       
    }
}
